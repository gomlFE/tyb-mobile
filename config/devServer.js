
// IP PORT 配置
const baseUrl = `http://127.0.0.1:9999`
// 设置代理
// netsh interface portproxy add v4tov4 listenport=9999 connectaddress=192.168.1.154 connectport=9999

// 显示所有代理
// netsh interface portproxy show all

// 删除配置: 本机的监听端口为10022,10022端口接受的连接地址为"*",使用的协议为tcp,当前仅支持TCP协议。
// netsh interface portproxy delete v4tov4 listenport=9999 listenaddress=* protocol=tcp
// 删除完毕。

// 代理转发表格
exports.proxy = {
  '/api': {
    target: baseUrl,
    changeOrigin: true,
    pathRewrite: {
      '^/api': ''
    }
  }
}
// 改成本地 IP
exports.host = '127.0.0.1'

exports.port = '8090'
