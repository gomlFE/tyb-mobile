const execSync = require('child_process').execSync
exports.getGitHash = function () {
  return execSync('git rev-parse HEAD').toString().trim()
}
