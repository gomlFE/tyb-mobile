import Vue from 'vue'
import Vuex from 'vuex'
import { getStore, setStore } from '@/util/store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    routeRecord: getStore({
      name: 'routeRecord'
    }),
    accessToken: getStore({
      name: 'accessToken'
    })
  },
  mutations: {
    SET_ROUTERECORD (state, routeRecord) {
      state.routeRecord = routeRecord
      setStore({
        name: 'routeRecord',
        content: state.routeRecord,
        type: 'session'
      })
    },
    SET_ACCESSTOKEN (state, accessToken) {
      state.accessToken = accessToken
      setStore({
        name: 'accessToken',
        content: state.accessToken,
        type: 'session'
      })
    }
  },
  actions: {
    SAVE_ACCESSTOKEN ({
      commit,
      state,
      dispatch
    }, token) {
      return new Promise((resolve, reject) => {
        commit('SET_ACCESSTOKEN', token)
        resolve()
      })
    }
  }
})
