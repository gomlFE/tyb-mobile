import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/fishing-boat-info'
    },
    {
      path: '/fishing-boat-transaction',
      name: 'fishing-boat-transaction',
      component: () => import('./views/fishing-boat-transaction/index'),
      children: [
        {
          path: '/fishing-boat-info',
          name: '渔船信息',
          component: () => import('./views/fishing-boat-transaction/fishing-boat-info')
        },
        {
          path: '/fishing-boat-publish',
          name: '发布信息',
          component: () => import('./views/fishing-boat-transaction/fishing-boat-publish')
        },
        {
          path: '/center',
          name: '我的',
          component: () => import('./views/center/index')
        }
      ]
    },
    {
      path: '/detail/sale-detail',
      name: 'detail/sale-detail',
      component: () => import('@/views/detail/sale-detail')
    },
    {
      path: '/detail/buying-detail',
      name: '详情',
      component: () => import('@/views/detail/buying-detail')
    }
  ]
})
