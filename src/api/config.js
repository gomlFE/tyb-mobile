import axios from 'axios'
import store from '../store'
// 超时时间
axios.defaults.timeout = 30000
axios.defaults.baseURL = 'http://api.taoyu58.com'
// 跨域请求，允许保存cookie
axios.defaults.withCredentials = false
// let msg
// HTTPrequest拦截
axios.interceptors.request.use(config => {
  if (store.state.accessToken) {
    config.headers = {
      'X-Requested-With': 'XMLHttpRequest',
      // ...config.headers,
      ...store.state.accessToken
    } // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
  }
  // console.log(config)
  return config
}, error => {
  return Promise.reject(error)
})
// HTTPresponse拦截
axios.interceptors.response.use(data => {
  return data
}, error => {
  return Promise.reject(new Error(error))
})

export default axios
