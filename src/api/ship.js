import request from '@/api/config'
import { homeMapUrl } from '@/api/mapRequestUrl'
const baseQuery = {
  pageSize: 20,
  id: null,
  shipname: null,
  buytype: 1,
  catid: null
}
export function getShipSaleList (query = baseQuery) {
  return request({
    url: homeMapUrl['ship/shiplist.html'],
    method: 'get',
    params: query
  })
}

export function getShipBuyingList (query = baseQuery) {
  return request({
    url: homeMapUrl['ship/shipbuylist.html'],
    method: 'get',
    params: query
  })
}

export function getShipSaleDetailList (query) {
  return request({
    url: homeMapUrl['ship/shipdetail.html'],
    method: 'get',
    params: query
  })
}

export function getShipBuyDetailList (query) {
  return request({
    url: homeMapUrl['ship/shipbuydetail.html'],
    method: 'get',
    params: query
  })
}
