import request from '@/api/config'
import { homeMapUrl } from '@/api/mapRequestUrl'
const baseQuery = {
  appId: '10777691',
  deviceId: 1,
  randStr: 'sdfsdf',
  timestamp: 7277557,
  signature: '80dbdf896c8296e70e30451edd6818bf'
}
export function getToken (query = baseQuery) {
  return request({
    url: homeMapUrl['token/accesstoken'],
    method: 'get',
    params: query
  })
}
