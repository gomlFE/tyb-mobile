import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as urls from './config/env'
import { getToken } from '@/api/token'

// CSS IMPORT
import './common/scss/index.scss'
import { loadStyle } from './util/util'
// mint-ui
import Mint from 'mint-ui'
import config from './config/global.js'

Vue.use(config)
Vue.use(Mint)

document.addEventListener('DOMContentLoaded', function () {
  if (window.FastClick) window.FastClick.attach(document.body)
}, false)

const { iconfontVersion, iconfontUrl } = urls
iconfontVersion.forEach(ele => {
  loadStyle(iconfontUrl.replace('$key', ele))
})

Vue.config.productionTip = false
Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})

router.beforeEach((to, from, next) => {
  // console.log(store.state.accessToken)
  if (store.state.accessToken) {
    next()
  } else {
    getToken().then(res => {
      store.dispatch('SAVE_ACCESSTOKEN', res.data.result).then(res => {
        next()
      })
    })
  }
})

router.afterEach((to, from) => {
  let routeRecord = { ...store.getters.routeRecord }
  routeRecord = {
    name: to.name,
    url: to.path
  }
  store.commit('SET_ROUTERECORD', routeRecord)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
