// 配置编译环境和线上环境之间的切换

let baseUrl = '/api'
// 最后一个icon URL只要替换即可, 无需添加
let iconfontVersion = ['898972_gkt7dohhuvi']
let iconfontUrl = `//at.alicdn.com/t/font_$key.css`
let codeUrl = `${baseUrl}/code`
const env = process.env
if (env.NODE_ENV === 'development') {
  baseUrl = `/api` // 开发环境地址
} else if (env.NODE_ENV === 'production') {
  baseUrl = `/api` // 生产环境地址
} else if (env.NODE_ENV === 'test') {
  baseUrl = `/api` // 测试环境地址
}
export {
  baseUrl,
  iconfontUrl,
  iconfontVersion,
  codeUrl,
  env
}
