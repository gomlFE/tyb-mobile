// let devServer
// try {
//   devServer = require('./config/devServer.local')
// } catch (e) {
//   devServer = require('./config/devServer')
// }
module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  productionSourceMap: false,
  // 配置转发代理
  devServer: {
    // host: devServer.host, // can be overwritten by process.env.HOST
    open: true,
    // port: devServer.port, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    // proxy: devServer.proxy,
    overlay: {
      warnings: true,
      errors: true
    }
  }
}
